-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2017 at 04:57 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_siswa`
--

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `nama` varchar(20) NOT NULL,
  `bahasa` int(11) NOT NULL,
  `matematik` int(11) NOT NULL,
  `komputer` int(11) NOT NULL,
  `rata` float NOT NULL,
  `grade` varchar(1) NOT NULL,
  `recommend` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`nama`, `bahasa`, `matematik`, `komputer`, `rata`, `grade`, `recommend`) VALUES
('Andika Bagus', 70, 66, 78, 71.3333, 'B', 'Harus belajar lagi yah'),
('Guntur Bumi Pangestu', 65, 98, 63, 75.3333, 'A', 'Harus belajar lagi yah'),
('Gibran Ikhwal', 90, 62, 86, 79.3333, 'A', 'Harus belajar lagi yah'),
('Sofyan Tindar', 67, 85, 52, 68, 'B', 'Harus belajar lagi yah'),
('Bayu Aji', 80, 72, 73, 75, 'A', 'Harus belajar lagi yah'),
('Pandi Sutrisno', 76, 65, 72, 71, 'B', 'Harus belajar lagi yah');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
