<?php
echo "<table>";
echo "<tr><th>Nama</th><th>Nilai Bahasa</th><th>Nilai Matematik</th><th>Nilai Komputer</th><th>Rata - Rata</th><th>Grade</th><th>Rekomendasi</th></tr>";

class TableRows extends RecursiveIteratorIterator{
	function __construct($it){
		parent::__construct($it, self::LEAVES_ONLY);
	}

	function current(){
		return "<td style='width:150px;border:1px solid black;'>" . parent::current(). "</td>";
	}

	function beginChildren(){
		echo "<tr>";
	}

	function endChildren(){
		echo "</tr>" . "\n";
	}
}

try {
    $handler = new PDO("mysql:host=localhost;dbname=db_siswa", "root", "");
    $handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = $handler->prepare("SELECT nama, bahasa, matematik, komputer, rata, grade, recommend FROM nilai");
    $query->execute();

    $result = $query->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new TableRows(new RecursiveArrayIterator($query->fetchAll())) as $k=>$v) {
        echo $v;
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
    die();
}
echo "</table>";
echo "<br>";
echo "<a href='index.php'><button>Kembali ke Form</button></a>";
?>